package samplefilechooser.main;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import samplefilechooser.action.FileManager;
import samplefilechooser.component.FileChooserJButton;
import samplefilechooser.component.FileContentJTextArea;
import samplefilechooser.component.PathJTextField;

public class MainFrame extends JFrame {
	
	private static JPanel pane;
	
	protected MainFrame() {
		super("FileChooserSampleApp");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new GridLayout(3, 1));
		
		PathJTextField pathField = new PathJTextField();
		FileContentJTextArea fileContentTextArea = new FileContentJTextArea();
		new FileManager(pathField, fileContentTextArea);
		JScrollPane scrollPane = new JScrollPane(fileContentTextArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		pane = (JPanel) this.getContentPane();
		pane.add(new FileChooserJButton());
		pane.add(pathField);
		pane.add(scrollPane);
	}
	
	public static JPanel getMainContentPane() {
		return pane;
	}

}

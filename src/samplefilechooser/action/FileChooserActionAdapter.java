package samplefilechooser.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

public class FileChooserActionAdapter extends AbstractAction {

	@Override
	public void actionPerformed(ActionEvent e) {
		FileManager.chooseFile();
	}

}

package samplefilechooser.action;

import java.io.File;

import javax.swing.JFileChooser;
import samplefilechooser.component.FileContentJTextArea;
import samplefilechooser.component.PathJTextField;
import samplefilechooser.main.MainFrame;

public class FileManager {
	
	private static PathJTextField pathField;
	private static FileContentJTextArea fileContentTextArea;
	private static String homePath;
	
	public FileManager(PathJTextField _pathField, FileContentJTextArea _fileContentTextArea) {
		pathField = _pathField;
		fileContentTextArea = _fileContentTextArea;
		homePath = System.getProperty("user.home") + "/Desktop";
	}
	
	protected static void chooseFile() {
		JFileChooser fileChooser = new JFileChooser(homePath);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fileChooser.setDialogTitle("ファイル選択");
		fileChooser.setFileFilter(new AppFileFilter());
		int ret = fileChooser.showOpenDialog(MainFrame.getMainContentPane());
		if (ret != JFileChooser.APPROVE_OPTION) return;
		File file = fileChooser.getSelectedFile();
		String path = file.getPath();
		homePath = path;
		setFilePath(path);
		appendFileContent(file);
	}
	
	private static void setFilePath(String path) {
		pathField.setFilePath(path);
	}
	
	private static void appendFileContent(File file) {
		String fileContent = FileReader.read(file);
		fileContentTextArea.appendFileContent(fileContent);
	}

}

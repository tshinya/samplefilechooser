package samplefilechooser.action;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class AppFileFilter extends FileFilter {
	
	private String[] extensions = {"txt", "csv"}; // 許可するファイル形式

	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		
		String name = f.getName().toLowerCase();
		for (int i = 0; i < extensions.length; i++) {
			if (name.endsWith(extensions[i])) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getDescription() {
		return "csv, txt";
	}

}

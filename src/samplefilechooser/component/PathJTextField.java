package samplefilechooser.component;

import javax.swing.JTextField;

public class PathJTextField extends JTextField {

	public PathJTextField() {
		super();
		this.setEditable(false);
	}
	
	public void setFilePath(String path) {
		this.setText(path);
	}
}

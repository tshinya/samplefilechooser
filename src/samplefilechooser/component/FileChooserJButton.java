package samplefilechooser.component;

import javax.swing.JButton;

import samplefilechooser.action.FileChooserActionAdapter;

public class FileChooserJButton extends JButton {
	
	public FileChooserJButton() {
		super("選択");
		this.addActionListener(new FileChooserActionAdapter());
	}

}

package samplefilechooser.component;

import javax.swing.JTextArea;

public class FileContentJTextArea extends JTextArea {
	
	public FileContentJTextArea() {
		super();
		this.setEditable(false);
	}
	
	public void appendFileContent(String fileContent) {
		this.append(fileContent);
	}

}
